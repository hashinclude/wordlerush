import { Request, Response, NextFunction, raw } from "express";
import { validationResult } from "express-validator";
import * as admin from "firebase-admin";
import { Room, RoomState } from "./resources";
import { CreateRoomRequest } from "./service";
import {
  validateNick,
  validateRoomConstraints,
  validateUserId,
} from "./validator";

export const validateCreateRoomRequest = () => {
  return [validateUserId("userId"), validateNick("nick")].concat(
    validateRoomConstraints("roomConstraints")
  );
};
export const createRoom = async (
  rawRequest: Request,
  response: Response,
  next: NextFunction
) => {
  const errors = validationResult(rawRequest);
  if (!errors.isEmpty()) {
    return response.status(400).json({ errors: errors.array() });
  }
  const request: CreateRoomRequest = rawRequest.body;
  if (
    request.roomConstraints.minDifficulty >
    request.roomConstraints.maxDifficulty
  ) {
    return response
      .status(400)
      .send("minDifficulty is greater than maxDifficulty");
  }

  const room: Room = {
    ownerUserId: request.userId,
    roomConstraints: request.roomConstraints,
    playerStates: [
      {
        userId: request.userId,
        nick: request.nick,
      },
    ],
    roomState: RoomState.INIT,
    lastUpdate: 0,
  };

  const roomId = await admin.firestore().collection("rooms").add(room);
  return roomId
    .update({ roomId: roomId.id, roomState: RoomState.CREATED })
    .then(() => {
      room.roomId = roomId.id;
      admin
        .database()
        .ref("rooms/" + room.roomId)
        .set(room.lastUpdate);
      response.status(200).send(room);
      return next();
    })
    .catch((err) => {
      return response.status(400).send(err.message);
    });
};
