export interface WordState {
  playerGuess?: string;
  matchString?: string;
}

export interface PlayerRoundState {
  wordStates?: Array<WordState>;
}

export interface PlayerState {
  userId: string;
  nick: string;
  rounds?: Array<PlayerRoundState>;
}

export interface RoomConstraints {
  maxNumPlayers: number;
  maxNumGuesses: number;
  wordLength: number;
  durationMillis: number;
  minDifficulty: number;
  maxDifficulty: number;
}

export enum RoomState {
  INIT = 0,
  CREATED = 1,
  STARTED = 2,
  ENDED = 3,
}
export interface Room {
  roomId?: string;
  ownerUserId?: string;
  playerStates?: Array<PlayerState>;
  roomConstraints: RoomConstraints;
  startTimeEpochMillis?: number;
  roomState: RoomState;
  answers?: Array<string>;
  lastUpdate: number;
}
