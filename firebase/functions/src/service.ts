import * as resources from "./resources";

export interface CreateRoomRequest {
  userId: string;
  nick: string;
  roomConstraints: resources.RoomConstraints;
}

export interface JoinRoomRequest {
  roomId: string;
  userId: string;
  nick: string;
}

export interface StartRoomRequest {
  userId: string;
  roomId: string;
}

export interface GetRoomRequest {
  userId: string;
  roomId: string;
}

export interface GuessRequest {
  userId: string;
  roomId: string;

  roundNum: number;
  guessNum: number;
  guess: string;
}
