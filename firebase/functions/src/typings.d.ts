interface Dictionary<T> {
  [Key: string]: T;
}

declare module "wordlist-english" {
  const wordlist: Dictionary<Array<string>>;
  export = wordlist;
}
