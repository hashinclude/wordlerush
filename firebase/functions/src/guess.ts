import { Request, Response, NextFunction } from "express";
import { body, validationResult } from "express-validator";
import * as admin from "firebase-admin";
import { DocumentSnapshot } from "firebase-functions/v1/firestore";
import { generateWord, isValidWord } from "./core";
import {
  PlayerRoundState,
  PlayerState,
  Room,
  RoomConstraints,
  RoomState,
  WordState,
} from "./resources";
import { GuessRequest } from "./service";
import { validateUserId } from "./validator";

export const validateGuessRequest = () => {
  return [validateUserId("userId"), body("guess").isAlpha()];
};
const validateGuess = (request: GuessRequest, rc: RoomConstraints) => {
  if (request.guess.length != rc.wordLength) {
    throw new Error("Invalid guess length");
  }
};
const createWordState = (
  request: GuessRequest,
  ps: PlayerState,
  rc: RoomConstraints
) => {
  const rounds: Array<PlayerRoundState> = ps.rounds || [];

  // Options are -
  // 1. This is the start of the game.
  if (rounds.length === 0) {
    if (request.roundNum === 0 && request.guessNum === 0) {
      ps.rounds = [{ wordStates: [{}] }];
      return ps.rounds[0].wordStates![0];
    } else {
      throw new Error("Invalid guess number");
    }
  }
  const lastRoundNum = rounds.length - 1;
  const lastRound = rounds[lastRoundNum];
  // 2. Middle of the game, player correctly guessed the last
  // word or is out of guesses for the last word.
  if ((lastRound.wordStates?.length || 0) >= rc.maxNumGuesses) {
    if (request.roundNum === lastRoundNum + 1 && request.guessNum === 0) {
      rounds.push({
        wordStates: [{}],
      });
      return rounds[rounds.length - 1].wordStates![0];
    } else {
      throw new Error("Invalid guess number");
    }
  }
  const lrWordStates = lastRound.wordStates || [];
  if (
    lrWordStates.length > 0 &&
    lrWordStates[lrWordStates.length - 1].matchString ==
      "G".repeat(rc.wordLength)
  ) {
    if (request.roundNum === lastRoundNum + 1 && request.guessNum === 0) {
      rounds.push({
        wordStates: [{}],
      });
      return rounds[rounds.length - 1].wordStates![0];
    } else {
      throw new Error("Invalid guess number");
    }
  }
  // 3. This is the middle of the game and the last element
  // in ps.rounds is what is being played.
  if (
    request.roundNum === lastRoundNum &&
    request.guessNum === lrWordStates.length
  ) {
    if (!rounds[rounds.length - 1].wordStates) {
      rounds[rounds.length - 1].wordStates = [];
    }
    rounds[rounds.length - 1].wordStates!.push({});
    return rounds[request.roundNum].wordStates![request.guessNum];
  } else {
    throw new Error("Invalid guess number");
  }
};

const applyGuess = (
  request: GuessRequest,
  wordState: WordState,
  answer: string
) => {
  wordState.playerGuess = request.guess;
  wordState.matchString = "";
  for (let i = 0; i < request.guess.length; i++) {
    const cc = request.guess[i];
    if (answer[i] == cc) {
      wordState.matchString += "G";
    } else if (answer.includes(cc)) {
      wordState.matchString += "Y";
    } else {
      wordState.matchString += "R";
    }
  }
};

export const guess = async (
  rawRequest: Request,
  response: Response,
  next: NextFunction
) => {
  const errors = validationResult(rawRequest);
  if (!errors.isEmpty()) {
    return response.status(400).json({ errors: errors.array() });
  }

  const request: GuessRequest = rawRequest.body;
  const roomRef = admin.firestore().collection("rooms").doc(request.roomId);
  if (!isValidWord(request.guess)) {
    return Promise.reject(new Error("Not a dictionary word: " + request.guess));
  }

  return admin
    .firestore()
    .runTransaction((txn) => {
      return txn.get(roomRef).then((roomDoc: DocumentSnapshot) => {
        if (!roomDoc.exists) {
          return Promise.reject(
            new Error("Room does not exist: " + request.roomId)
          );
        }
        const room: Room = roomDoc.data() as Room;
        const ps = room.playerStates?.find((ps) => ps.userId == request.userId);
        if (!ps) {
          return Promise.reject(
            new Error("Player does not have permission to view this room.")
          );
        }
        if (
          room.roomState != RoomState.ENDED &&
          (room.startTimeEpochMillis || Date.now()) +
            room.roomConstraints.durationMillis <
            Date.now()
        ) {
          room.roomState = RoomState.ENDED;
          txn.set(roomRef, room);
          return room;
        } else if (room.roomState != RoomState.STARTED) {
          return Promise.reject(
            new Error(
              "Cannot guess while room is in state: " +
                RoomState[room.roomState]
            )
          );
        }
        validateGuess(request, room.roomConstraints);
        const wordState = createWordState(request, ps, room.roomConstraints);
        if (room.answers!.length == request.roundNum) {
          room.answers!.push(
            generateWord(
              room.roomConstraints.wordLength,
              room.roomConstraints.minDifficulty,
              room.roomConstraints.maxDifficulty
            )
          );
        }
        applyGuess(request, wordState, room.answers![request.roundNum]);
        room.lastUpdate += 1;
        txn.set(roomRef, room);
        return room;
      });
    })
    .then((room: Room) => {
      admin
        .database()
        .ref("rooms/" + room.roomId)
        .set(room.lastUpdate);

      response.status(200).send(room);
      return next();
    })
    .catch((err: Error) => {
      response.status(400).send(err.message);
    });
};
