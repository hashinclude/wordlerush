import {Request, Response, NextFunction} from "express";
import {validationResult} from "express-validator";
import * as admin from "firebase-admin";
import {DocumentSnapshot} from "firebase-functions/v1/firestore";
import {nickFromUserId} from "./core";
import {Room, RoomState} from "./resources";
import {JoinRoomRequest} from "./service";
import {validateNick, validateUserId} from "./validator";

export const validateJoinRoomRequest = () => {
  return [validateUserId("userId"), validateNick("nick")];
};
export const joinRoom = async (
    rawRequest: Request,
    response: Response,
    next: NextFunction
) => {
  const errors = validationResult(rawRequest);
  if (!errors.isEmpty()) {
    return response.status(400).json({errors: errors.array()});
  }

  const request: JoinRoomRequest = rawRequest.body;
  const roomRef = admin.firestore().collection("rooms").doc(request.roomId);

  return admin
      .firestore()
      .runTransaction((txn) => {
        return txn.get(roomRef).then((roomDoc: DocumentSnapshot) => {
          if (!roomDoc.exists) {
            return Promise.reject(
                new Error("Room does not exist: " + request.roomId)
            );
          }
          const room: Room = roomDoc.data() as Room;
          if (room.playerStates?.find((ps) => ps.userId == request.userId)) {
            return room;
          }
          if (
            room.playerStates &&
          room.playerStates?.length >= room.roomConstraints.maxNumPlayers
          ) {
            return Promise.reject(new Error("Room has reached player limit."));
          }
          if (room.roomState != RoomState.CREATED) {
            return Promise.reject(
                new Error(
                    "Player cannot join when room is in state: " +
                RoomState[room.roomState]
                )
            );
          }
        room.playerStates?.push({
          userId: request.userId,
          nick: request.nick,
        });
        room.lastUpdate += 1;
        txn.set(roomRef, room);
        return room;
        });
      })
      .then((room: Room) => {
        admin
            .database()
            .ref("rooms/" + room.roomId)
            .set(room.lastUpdate);
        response.status(200).send(room);
        return next();
      })
      .catch((err) => {
        response.status(400).send(err.message);
      });
};
