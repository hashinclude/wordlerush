import { Request, Response, NextFunction } from "express";
import { validationResult } from "express-validator";
import * as admin from "firebase-admin";
import { DocumentSnapshot } from "firebase-functions/v1/firestore";
import { Room, RoomState } from "./resources";
import { GetRoomRequest } from "./service";
import { validateUserId } from "./validator";

export const validateGetRoomRequest = () => {
  return [validateUserId("userId")];
};
export const getRoom = async (
  rawRequest: Request,
  response: Response,
  next: NextFunction
) => {
  const errors = validationResult(rawRequest);
  if (!errors.isEmpty()) {
    return response.status(400).json({ errors: errors.array() });
  }

  const request: GetRoomRequest = rawRequest.body;
  const roomRef = admin.firestore().collection("rooms").doc(request.roomId);

  return admin
    .firestore()
    .runTransaction((txn) => {
      return txn.get(roomRef).then((roomDoc: DocumentSnapshot) => {
        if (!roomDoc.exists) {
          return Promise.reject(
            new Error("Room does not exist: " + request.roomId)
          );
        }
        const room: Room = roomDoc.data() as Room;
        if (!room.playerStates?.find((ps) => ps.userId == request.userId)) {
          return Promise.reject(
            new Error("Player does not have permission to view this room.")
          );
        }
        if (
          room.roomState != RoomState.ENDED &&
          (room.startTimeEpochMillis || Date.now()) +
            room.roomConstraints.durationMillis <
            Date.now()
        ) {
          room.roomState = RoomState.ENDED;
          txn.set(roomRef, room);
        }
        return room;
      });
    })
    .then((room: Room) => {
      response.status(200).send(room);
      return next();
    })
    .catch((err) => {
      response.status(400).send(err.message);
    });
};
