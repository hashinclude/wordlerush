import ev = require("express-validator");
const body = ev.body;

export const validateUserId = (userId: string) => {
  return body(userId, "userId must be alphanum and have a length of 10")
    .isAlphanumeric()
    .isLength({ min: 10, max: 10 });
};

export const validateNick = (nick: string) => {
  return body(nick, "nick must be alphanum and have a length of (4, 10)")
    .isAlphanumeric()
    .isLength({ min: 4, max: 10 });
};

export const validateRoomConstraints = (rc: string) => {
  return [
    body(rc + ".maxNumPlayers", "maxNumPlayers must be between 1 and 8").isInt({
      min: 1,
      max: 8,
    }),
    body(rc + ".maxNumGuesses", "maxNumGuesses must be between 4 and 10").isInt(
      { min: 4, max: 10 }
    ),
    body(
      rc + ".durationMillis",
      "durationMillis must be between 1 minute and 5 minutes"
    ).isInt({
      min: 60000,
      max: 3600000,
    }),
    body(rc + ".wordLength", "wordLength must be between 4 and 8").isInt({
      min: 4,
      max: 8,
    }),
    body(rc + ".minDifficulty", "minDifficulty must be between 1 and 8").isInt({
      min: 1,
      max: 8,
    }),
    body(
      rc + ".maxDifficulty",
      "maxDifficulty must be between minDifficulty and 8"
    ).isInt({
      min: 1,
      max: 8,
    }),
  ];
};
