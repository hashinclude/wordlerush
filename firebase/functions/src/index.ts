import * as functions from "firebase-functions";
import * as admin from "firebase-admin";
import express, { NextFunction } from "express";
import { createRoom, validateCreateRoomRequest } from "./create_room";
import { joinRoom, validateJoinRoomRequest } from "./join_room";
import { startRoom, validateStartRoomRequest } from "./start_room";
import { getRoom, validateGetRoomRequest } from "./get_room";
import { guess, validateGuessRequest } from "./guess";

admin.initializeApp();

// Start writing Firebase Functions
// https://firebase.google.com/docs/functions/typescript

const corsf = (req: any, res: any, next: NextFunction) => {
  // update to match the domain you will make the request from
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
};
const app = express();
app.use(corsf);
app.post("/createRoom", validateCreateRoomRequest(), createRoom);
app.post("/joinRoom", validateJoinRoomRequest(), joinRoom);
app.post("/startRoom", validateStartRoomRequest(), startRoom);
app.post("/getRoom", validateGetRoomRequest(), getRoom);
app.post("/guess", validateGuessRequest(), guess);

const main = express();
main.use("/service", app);

exports.main = functions.https.onRequest(main);
