import _ from "lodash";
import { PlayerState } from "./resources";
// import { wordlist } from "wordlist-english";
import wordlist = require("wordlist-english");
import wordList2Path = require("word-list");
import fs = require("fs");

// [difficulty][length][i]
const wordListByDiffAndLength: Array<Array<Array<string>>> = new Array(10);
let wordList2: Array<string>;

const generateMap = () => {
  wordList2 = fs.readFileSync(wordList2Path, "utf8").split("\n");
  const difficulties = [10, 20, 35, 40, 50, 55, 60, 70];
  for (let i = 0; i < difficulties.length; i++) {
    wordListByDiffAndLength[i] = new Array(9);
    for (let j = 4; j <= 8; j++) {
      wordListByDiffAndLength[i][j] = [];
    }
  }
  for (let dindex = 0; dindex < difficulties.length; dindex++) {
    for (const word of wordlist[
      "english/".concat(difficulties[dindex].toString())
    ]) {
      if (word.length >= 4 && word.length <= 8) {
        wordListByDiffAndLength[dindex][word.length].push(word);
      }
    }
  }
};
generateMap();

export const nickFromUserId = (
  playerStates: Array<PlayerState>,
  userId: string
) => {
  const player = playerStates.find((ps) => userId === ps.userId);
  return player && player.nick;
};

const getRandomInt = (min: number, max: number) => {
  min = Math.ceil(min);
  max = Math.floor(max) + 1;
  return Math.floor(Math.random() * (max - min) + min);
};

export const generateWord = (
  wordLength: number,
  minDifficulty: number,
  maxDifficulty: number
) => {
  minDifficulty = Math.max(minDifficulty - 1, 0);
  maxDifficulty = Math.min(maxDifficulty, wordListByDiffAndLength.length) - 1;
  if (wordLength < 4 || wordLength > 8)
    throw new Error("Invalid word Length: " + wordLength);
  const cd =
    wordListByDiffAndLength[getRandomInt(minDifficulty, maxDifficulty)][
      wordLength
    ];
  const windex = getRandomInt(0, cd.length - 1);
  return cd[windex];
};

export const isValidWord = (word: string) => {
  console.log(word);
  return (
    wordlist["english"].indexOf(word) != -1 || wordList2.indexOf(word) != -1
  );
};
