import { Request, Response, NextFunction } from "express";
import { validationResult } from "express-validator";
import * as admin from "firebase-admin";
import { DocumentSnapshot } from "firebase-functions/v1/firestore";
import { generateWord, nickFromUserId } from "./core";
import { Room, RoomState } from "./resources";
import { StartRoomRequest } from "./service";
import { validateUserId } from "./validator";

export const validateStartRoomRequest = () => {
  return [validateUserId("userId")];
};
export const startRoom = async (
  rawRequest: Request,
  response: Response,
  next: NextFunction
) => {
  const errors = validationResult(rawRequest);
  if (!errors.isEmpty()) {
    return response.status(400).json({ errors: errors.array() });
  }

  const request: StartRoomRequest = rawRequest.body;
  const roomRef = admin.firestore().collection("rooms").doc(request.roomId);

  return admin
    .firestore()
    .runTransaction((txn) => {
      return txn.get(roomRef).then((roomDoc: DocumentSnapshot) => {
        if (!roomDoc.exists) {
          return Promise.reject(
            new Error("Room does not exist: " + request.roomId)
          );
        }
        const room: Room = roomDoc.data() as Room;
        if (room.ownerUserId != request.userId) {
          return Promise.reject(
            new Error(
              "Only room owner can start the round: " +
                nickFromUserId(room.playerStates || [], request.userId)
            )
          );
        }
        if (room.roomState != RoomState.CREATED) {
          return Promise.reject(
            new Error(
              "Room is not in the right state to start: " +
                RoomState[room.roomState]
            )
          );
        }
        room.roomState = RoomState.STARTED;
        room.startTimeEpochMillis = Date.now();
        room.answers = [
          generateWord(
            room.roomConstraints.wordLength,
            room.roomConstraints.minDifficulty,
            room.roomConstraints.maxDifficulty
          ),
        ];
        room.lastUpdate += 1;
        txn.set(roomRef, room);
        return room;
      });
    })
    .then((room: Room) => {
      admin
        .database()
        .ref("rooms/" + room.roomId)
        .set(room.lastUpdate);

      response.status(200).send(room);
      return next();
    })
    .catch((err) => {
      response.status(400).send(err.message);
    });
};
