import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { map } from 'rxjs';
import { Room } from '../room';
import { WordlerushService } from '../wordlerush.service';

function randomString(length: number, chars: string) {
  var result = '';
  for (var i = length; i > 0; --i)
    result += chars[Math.floor(Math.random() * chars.length)];
  return result;
}

function extractError(e: HttpErrorResponse) {
  if (e.error.errors && e.error.errors.length > 0) {
    return e.error.errors[0].msg;
  }
  return e.error;
}

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  encapsulation: ViewEncapsulation.None,
})
export class HomeComponent implements OnInit {
  nick = new FormControl('', [
    Validators.required,
    Validators.maxLength(10),
    Validators.pattern('[a-zA-z0-9]*'),
  ]);

  numGuesses = new FormControl('', [
    Validators.required,
    Validators.min(4),
    Validators.max(10),
  ]);
  durationMinutes = new FormControl('', [
    Validators.required,
    Validators.min(1),
    Validators.max(10),
  ]);
  maxPlayers = new FormControl('', [
    Validators.required,
    Validators.min(1),
    Validators.max(8),
  ]);
  wordLength = new FormControl('', [
    Validators.required,
    Validators.min(4),
    Validators.max(8),
  ]);
  minDifficulty = new FormControl('', [
    Validators.required,
    Validators.min(1),
    Validators.max(8),
  ]);
  maxDifficulty = new FormControl('', [
    Validators.required,
    Validators.min(1),
    Validators.max(8),
  ]);
  joinRoomId = new FormControl('', []);
  constructor(
    private wordleRushService: WordlerushService,
    private router: Router,
    private snackBar: MatSnackBar
  ) {}

  createAndGoToRoom() {
    if (
      this.nick.valid &&
      this.numGuesses.valid &&
      this.durationMinutes.valid &&
      this.maxPlayers.valid &&
      this.wordLength.valid
    ) {
      this.wordleRushService
        .createRoom({
          durationMillis: this.durationMinutes.value * 60000,
          maxNumGuesses: this.numGuesses.value * 1,
          maxNumPlayers: this.maxPlayers.value * 1,
          wordLength: this.wordLength.value * 1,
          minDifficulty: this.minDifficulty.value * 1,
          maxDifficulty: this.maxDifficulty.value * 1,
        })
        .subscribe(
          (response) => {
            this.router.navigateByUrl('/room?id=' + (response as Room).roomId);
          },
          (error) => {
            this.snackBar.open(extractError(error), 'Ack', {
              duration: 3000,
            });
          }
        );
    }
  }

  joinAndGoToRoom() {
    if (!this.joinRoomId.value) return;
    this.wordleRushService.joinRoom(this.joinRoomId.value).subscribe(
      (response) => {
        this.router.navigateByUrl('/room?id=' + (response as Room).roomId);
      },
      (error) => {
        this.snackBar.open(extractError(error), 'Ack', {
          duration: 3000,
        });
      }
    );
  }
  setupFormField(field: FormControl, name: string, def: any) {
    field.valueChanges.subscribe(() => {
      if (field.valid) {
        localStorage.setItem(name, field.value);
      }
    });
    field.setValue(localStorage.getItem(name) || def);
  }
  ngOnInit(): void {
    this.setupFormField(this.nick, 'nick', '');
    this.setupFormField(this.numGuesses, 'numGuesses', 8);
    this.setupFormField(this.durationMinutes, 'durationMinutes', 1);
    this.setupFormField(this.maxPlayers, 'maxPlayers', 8);
    this.setupFormField(this.wordLength, 'wordLength', 5);
    this.setupFormField(this.minDifficulty, 'minDifficulty', 1);
    this.setupFormField(this.maxDifficulty, 'maxDifficulty', 3);

    const uid = localStorage.getItem('userId');
    if (!uid) {
      var rString = randomString(10, '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ');
      localStorage.setItem('userId', rString);
    }
  }
}
