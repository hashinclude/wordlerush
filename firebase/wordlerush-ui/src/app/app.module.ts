import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatDividerModule } from '@angular/material/divider';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { ReactiveFormsModule } from '@angular/forms';
import { MatListModule } from '@angular/material/list';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatSliderModule } from '@angular/material-experimental/mdc-slider';
import { MatTabsModule } from '@angular/material/tabs';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { HttpClientModule } from '@angular/common/http';
import { SinglePlayerComponent } from './single-player/single-player.component';
import { RouterModule, Routes } from '@angular/router';
import { RoundProgressModule } from 'angular-svg-round-progressbar';
import { WordComponent } from './word/word.component';
import { HomeComponent } from './home/home.component';
import { RoomComponent } from './room/room.component';
import { provideFirebaseApp, getApp, initializeApp } from '@angular/fire/app';
import { RanklistComponent } from './single-player/ranklist/ranklist.component';
import { environment } from 'src/environments/environment';

const routes: Routes = [
  { path: 'room', component: RoomComponent },
  { path: 'play', component: SinglePlayerComponent },
  { path: '', component: HomeComponent },
];
@NgModule({
  declarations: [
    AppComponent,
    SinglePlayerComponent,
    WordComponent,
    HomeComponent,
    RoomComponent,
    RanklistComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MatToolbarModule,
    MatIconModule,
    MatListModule,
    MatSnackBarModule,
    MatTabsModule,
    MatGridListModule,
    MatInputModule,
    MatButtonModule,
    MatSidenavModule,
    MatSliderModule,
    MatCardModule,
    MatDividerModule,
    RoundProgressModule,
    ReactiveFormsModule,
    RouterModule.forRoot(routes),
    provideFirebaseApp(() => initializeApp(environment.firebase)),
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
