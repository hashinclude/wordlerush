export interface WordState {
  playerGuess?: string;
  matchString?: string;
}

export interface PlayerRoundState {
  wordStates?: Array<WordState>;
}

export interface PlayerState {
  userId: string;
  nick: string;
  rounds?: Array<PlayerRoundState>;
}

export interface RoomConstraints {
  maxNumPlayers: number;
  maxNumGuesses: number;
  wordLength: number;
  durationMillis: number;
  minDifficulty: number;
  maxDifficulty: number;
}

export enum RoomState {
  INIT = 0,
  CREATED = 1,
  STARTED = 2,
  ENDED = 3,
}
export interface Room {
  roomId?: string;
  ownerUserId?: string;
  playerStates?: Array<PlayerState>;
  roomConstraints: RoomConstraints;
  startTimeEpochMillis?: number;
  roomState: RoomState;
  answers?: Array<string>;
}

export const NickFromUserId = (
  player_states: Array<PlayerState>,
  userId: string
) => {
  const player = player_states.find((ps) => userId === ps.userId);
  return player && player.nick;
};

export const scoreFromPlayerState = (ps: PlayerState) => {
  var score = 0;
  for (var i = 0; i < (ps.rounds?.length || 0); i++) {
    if (!ps.rounds![i].wordStates) continue;
    const ws = ps.rounds![i].wordStates!;
    if (
      ws[ws.length - 1].matchString ===
      'G'.repeat(ws[0]?.matchString?.length || 0)
    ) {
      score += 1;
    }
  }
  return score;
};
