import {
  Component,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
} from '@angular/core';

export interface Character {
  value: string;
  color: string;
}

@Component({
  selector: 'app-word',
  templateUrl: './word.component.html',
  styleUrls: ['./word.component.scss'],
})
export class WordComponent implements OnChanges {
  @Input() word = '';
  @Input() matchString = '';
  @Input() isCurrent = false;

  chars: Array<Character> = [];
  colorMap = new Map<string, string>([
    ['G', '#bdff94'],
    ['Y', '#fce29a'],
    ['R', 'rgba(200, 200, 200, 0.541)'],
  ]);
  ngOnChanges(changes: SimpleChanges): void {
    const new_chars: Array<Character> = [];
    for (var i = 0; i < this.word.length; i++) {
      new_chars.push({
        value: this.word[i],
        color: this.colorMap.get(this.matchString[i])!,
      });
    }
    this.chars = new_chars;
  }
}
