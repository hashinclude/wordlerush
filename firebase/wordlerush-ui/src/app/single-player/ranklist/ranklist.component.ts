import { Component, Input, OnInit } from '@angular/core';
import { FirebaseApp } from '@angular/fire/app';
import { Router } from '@angular/router';
import { initializeApp } from 'firebase/app';
import { Database, getDatabase, onValue, ref } from 'firebase/database';
import { Room, RoomState, scoreFromPlayerState } from 'src/app/room';
import { WordlerushService } from 'src/app/wordlerush.service';
import { environment } from 'src/environments/environment.prod';

class Score {
  score: number = 0;
  nick: string = '';
}
@Component({
  selector: 'app-ranklist',
  templateUrl: './ranklist.component.html',
  styleUrls: ['./ranklist.component.scss'],
})
export class RanklistComponent implements OnInit {
  @Input() roomId?: string;
  room?: Room;
  scores: Array<Score> = [];
  subscription?: any;
  db?: Database;

  constructor(
    private wordleRushService: WordlerushService,
    private router: Router,
    private firebase: FirebaseApp
  ) {}

  updateScores() {
    const new_scores: Array<Score> = [];
    for (let ps of this.room!.playerStates!) {
      new_scores.push({ score: scoreFromPlayerState(ps), nick: ps.nick });
    }
    new_scores.sort((s1: Score, s2: Score) => {
      return s2.score - s1.score;
    });
    this.scores = new_scores;
  }

  updateRoomState() {
    console.log('Updating room: ' + this.roomId);
    if (!this.roomId) return;
    this.wordleRushService.getRoom(this.roomId).subscribe(
      (res) => {
        this.room = <Room>res;
        this.updateScores();
        if (
          this.room?.roomState == RoomState.STARTED ||
          this.room?.roomState == RoomState.ENDED
        ) {
          this.router.navigateByUrl('/play?id=' + this.roomId);
        }
      },
      (err) => console.log(err)
    );
  }
  setupSubscription() {
    if (!this.roomId) return;
    if (this.subscription) this.subscription.cancel();
    const roomRef = ref(this.db!, 'rooms/' + this.roomId);
    this.updateRoomState();
    this.subscription = onValue(roomRef, (snapshot) => {
      this.updateRoomState();
    });
  }

  ngOnInit(): void {
    this.db = getDatabase(this.firebase);
    this.setupSubscription();
  }
  ngOnChange(): void {
    this.setupSubscription();
  }
}
