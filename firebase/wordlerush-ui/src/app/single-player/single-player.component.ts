import { HostListener, Input, ViewEncapsulation } from '@angular/core';
import { AfterViewInit, Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { interval } from 'rxjs';
import Keyboard from 'simple-keyboard';
import { PlayerState, Room, scoreFromPlayerState } from '../room';
import { WordlerushService } from '../wordlerush.service';

export interface Guess {
  guess: string[];
  matchString: string;
  isCurrent: boolean;
}
@Component({
  selector: 'app-single-player',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './single-player.component.html',
  styleUrls: ['./single-player.component.scss'],
})
export class SinglePlayerComponent implements OnInit {
  keyboard!: Keyboard;
  guesses: Array<Guess> = [];
  curWord = 0;
  curRound = 0;
  curWordCursor = 0;
  room?: Room;
  roomId?: string;
  remainingTimeMillis: number = 0;
  score = 0;
  disabledChars = new Set();
  roundHistory: Array<string> = [];

  constructor(
    private wordlerushService: WordlerushService,
    private route: ActivatedRoute,
    private router: Router,
    private snackBar: MatSnackBar
  ) {}

  ngOnInit(): void {
    this.keyboard = new Keyboard({
      layout: {
        default: [
          'Q W E R T Y U I O P {bksp}',
          'A S D F G H J K L',
          'Z X C V B N M {enter}',
        ],
      },
      onKeyPress: (button: string) => this.onKeyPress(button),
    });
    interval(1000).subscribe(
      (x) =>
        (this.remainingTimeMillis = Math.max(
          0,
          (this.room?.startTimeEpochMillis || 0) +
            (this.room?.roomConstraints.durationMillis || 0) -
            Date.now()
        ))
    );
    this.route.queryParams.subscribe((params) => {
      this.roomId = params['id'];
      this.updateRoomState();
    });
  }
  setRoomState(new_room: Room) {
    this.room = new_room;
    const rc = this.room!.roomConstraints;
    const myState = this.getMyPlayerState()!;
    const [curRound, curGuess] = this.getCurrentRound(myState);
    this.curWord = curGuess;
    this.curRound = curRound;
    this.guesses = [];
    this.score = scoreFromPlayerState(myState);
    this.disabledChars.clear();
    for (var i = 0; i < rc.maxNumGuesses; i++) {
      if (i >= curGuess) {
        this.guesses.push({
          guess: [...'_'.repeat(rc.wordLength)],
          matchString: 'S'.repeat(rc.wordLength),
          isCurrent: i == curGuess,
        });
      } else {
        const ws = myState.rounds![curRound].wordStates![i]!;
        for (var j = 0; j < rc.wordLength; j++) {
          if (ws.matchString![j] == 'R') {
            this.disabledChars.add(ws.playerGuess![j]);
          }
        }
        this.guesses.push({
          guess: ws.playerGuess!.toUpperCase().split(''),
          matchString: ws.matchString!,
          isCurrent: false,
        });
      }
    }
    this.keyboard.removeButtonTheme(
      'Q W E R T Y U I O P A S D F G H J K L Z X C V B N M',
      ''
    );
    this.keyboard.addButtonTheme(
      Array.from(this.disabledChars).join(' ').toUpperCase(),
      'hg-disable'
    );
    this.roundHistory = [];
    for (var i = 0; i < this.curRound; i++) {
      const ws = myState.rounds![i].wordStates!;
      if (ws[ws.length - 1].matchString == 'G'.repeat(rc.wordLength)) {
        this.roundHistory.push('W');
      } else {
        this.roundHistory.push('L');
      }
    }

    this.curWordCursor = 0;
  }
  getCurrentRound(ps: PlayerState) {
    if (!ps) return [0, 0];
    if (!ps.rounds) return [0, 0];
    const rlen = ps.rounds!.length;
    if (!rlen) return [0, 0];
    const ws = ps.rounds![rlen - 1].wordStates;
    if (!ws) return [rlen - 1, 0];
    if (!ws.length) return [rlen - 1, 0];
    if (
      ws[ws.length - 1].matchString ==
        'G'.repeat(this.room!.roomConstraints.wordLength) ||
      ws.length == this.room!.roomConstraints.maxNumGuesses
    ) {
      return [rlen, 0];
    }
    return [rlen - 1, ws.length];
  }
  getMyPlayerState() {
    if (!this.room) return undefined;
    const myState = this.room.playerStates?.find(
      (ps) => ps.userId == localStorage.getItem('userId')
    );
    return myState;
  }
  updateRoomState() {
    this.wordlerushService
      .getRoom(this.roomId!)
      .subscribe(
        (res) => (
          this.setRoomState(res as Room), (err: any) => console.log(err)
        )
      );
  }

  @HostListener('document:keydown', ['$event'])
  handleKeyboardEvent(event: KeyboardEvent) {
    if (!this.room) return;
    if (event.key === 'Backspace') {
      return this.onBackSpace();
    }
    if (event.key === 'Enter') {
      return this.onEnter();
    }
    if (/^[A-Z]$/i.test(event.key)) {
      return this.onAlpha(event.key.toUpperCase());
    }
  }

  onBackSpace = () => {
    if (!this.room) return;
    if (this.curWordCursor === 0) return;
    this.curWordCursor -= 1;
    this.guesses[this.curWord].guess[this.curWordCursor] = '_';
    return;
  };
  onAlpha = (button: string) => {
    if (!this.room) return;
    if (this.curWordCursor === this.room!.roomConstraints.wordLength) return;
    this.guesses[this.curWord].guess[this.curWordCursor] = button;
    this.curWordCursor += 1;
  };
  onEnter = () => {
    if (!this.room) return;
    if (
      this.guesses[this.curWord].guess[
        this.room!.roomConstraints.wordLength - 1
      ] === '_'
    )
      return;
    this.wordlerushService
      .runGuess(
        this.roomId!,
        this.curRound,
        this.curWord,
        this.guesses[this.curWord].guess.join('')
      )
      .subscribe(
        (response) => {
          this.setRoomState(response as Room);
        },
        (error) => {
          console.log(error);
          this.snackBar.open(error.error, 'Ack', { duration: 5000 });
        }
      );
  };
  onKeyPress = (button: string) => {
    if (!this.room) return;
    if (button === '{bksp}') {
      return this.onBackSpace();
    }
    if (button === '{enter}') {
      return this.onEnter();
    }
    return this.onAlpha(button);
  };
}
