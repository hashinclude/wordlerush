import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { interval } from 'rxjs';
import { Room, RoomConstraints, RoomState } from '../room';
import { WordlerushService } from '../wordlerush.service';
import { getMessaging, getToken, onMessage } from 'firebase/messaging';
import { FirebaseApp } from '@angular/fire/app';
import {
  connectDatabaseEmulator,
  getDatabase,
  onValue,
  ref,
  set,
} from 'firebase/database';
import { initializeApp } from 'firebase/app';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-room',
  templateUrl: './room.component.html',
  styleUrls: ['./room.component.scss'],
})
export class RoomComponent implements OnInit {
  room?: Room;
  roomId?: string;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private wordleRushService: WordlerushService,
    private snackBar: MatSnackBar,
    private firebase: FirebaseApp
  ) {}

  getPlayerNicks() {
    if (!this.room) return [];
    const res: Array<string> = [];
    for (var i = 0; i < this.room.roomConstraints.maxNumPlayers; i++) {
      if (i < this.room.playerStates!.length) {
        res.push(this.room.playerStates![i].nick);
      } else {
        res.push('<WAITING>');
      }
    }
    return res;
  }

  updateRoomState() {
    console.log('Updating room.');
    if (!this.roomId) return;
    this.wordleRushService.getRoom(this.roomId).subscribe(
      (res) => {
        this.room = <Room>res;
        if (
          this.room?.roomState == RoomState.STARTED ||
          this.room?.roomState == RoomState.ENDED
        ) {
          this.router.navigateByUrl('/play?id=' + this.roomId);
        }
      },
      (err) => console.log(err)
    );
  }
  startRoom() {
    if (!this.room) return;
    this.wordleRushService.startRoom(this.roomId!).subscribe(
      (res) => this.router.navigateByUrl('/play?id=' + this.roomId),
      (err) => {
        this.snackBar.open(
          "Cannot Start. Make sure you're the room owner.",
          'Ack',
          { duration: 3000 }
        );
        console.log(err);
      }
    );
  }
  ngOnInit(): void {
    this.route.queryParams.subscribe((params) => {
      this.roomId = params['id'];
      this.updateRoomState();
      const db = getDatabase(this.firebase);
      const roomRef = ref(db, 'rooms/' + this.roomId);
      console.log(roomRef);
      onValue(roomRef, (snapshot) => {
        console.log(snapshot);
        this.updateRoomState();
      });
    });
    // interval(10000).subscribe((x) => this.updateRoomState());
  }
}
