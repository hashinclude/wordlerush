import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { RoomConstraints } from './room';

@Injectable({
  providedIn: 'root',
})
export class WordlerushService {
  root = '/service/';

  headers = { 'content-type': 'application/json' };
  constructor(private http: HttpClient) {
    if (location.hostname === 'localhost') {
      this.root = 'http://localhost:5000/service/';
    }
  }

  createRoom(rc: RoomConstraints) {
    return this.http.post(
      this.root + 'createRoom',
      JSON.stringify({
        userId: localStorage.getItem('userId')!,
        nick: localStorage.getItem('nick')!,
        roomConstraints: rc,
      }),
      { headers: this.headers }
    );
  }

  joinRoom(roomId: string) {
    return this.http.post(
      this.root + 'joinRoom',
      JSON.stringify({
        userId: localStorage.getItem('userId')!,
        roomId: roomId,
        nick: localStorage.getItem('nick'),
      }),
      { headers: this.headers }
    );
  }

  startRoom(roomId: string) {
    return this.http.post(
      this.root + 'startRoom',
      JSON.stringify({
        userId: localStorage.getItem('userId')!,
        roomId: roomId,
      }),
      { headers: this.headers }
    );
  }

  getRoom(roomId: string) {
    return this.http.post(
      this.root + 'getRoom',
      JSON.stringify({
        userId: localStorage.getItem('userId')!,
        roomId: roomId,
      }),
      { headers: this.headers }
    );
  }
  runGuess(roomId: string, roundNum: number, guessNum: number, guess: string) {
    return this.http.post(
      this.root + 'guess',
      JSON.stringify({
        userId: localStorage.getItem('userId')!,
        roomId: roomId,
        roundNum: roundNum,
        guessNum: guessNum,
        guess: guess.toLowerCase(),
      }),
      { headers: this.headers }
    );
  }
}
