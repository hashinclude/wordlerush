import { TestBed } from '@angular/core/testing';

import { WordlerushService } from './wordlerush.service';

describe('WordlerushServiceService', () => {
  let service: WordlerushService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(WordlerushService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
