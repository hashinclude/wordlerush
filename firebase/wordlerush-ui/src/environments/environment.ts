// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    name: 'WordleRush',
    options: {},
    databaseURL: 'http://localhost:9000/?ns=wordlerush-default-rtdb',
    automaticDataCollectionEnabled: false,
    apiKey: 'AIzaSyAylNc5LlhsQ2ekcWx0KrZyan3AmBou0ts',
    authDomain: 'wordlerush.firebaseapp.com',
    projectId: 'wordlerush',
    storageBucket: 'wordlerush.appspot.com',
    messagingSenderId: '809817890348',
    appId: '1:809817890348:web:c07fa4fe112e86afe3fb89',
    measurementId: 'G-1MYEKPY0N9',
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
